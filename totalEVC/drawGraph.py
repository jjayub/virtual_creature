import matplotlib.pyplot as plt




plt.title('Fitness')
plt.ylabel('Max Fitness')
plt.xlabel('Generation #')

foldername = 'exp3'
numOfRuns = 10
numOfGens = 20
xar = []
for i in range(0, numOfGens):
    xar.append(i)

for run in range(0, numOfRuns):
    for i in range(0, numOfGens):
        yar = []
        filename = foldername + '/run' + str(run) + '_fitness.txt'
        pullData = open(filename, 'r').read()
        dataArray = pullData.split('\n')
        for eachLine in dataArray:
            if len(eachLine) > 1:
                x, y = eachLine.split(',')
                yar.append(float(y))
        plt.plot(xar, yar)


plt.show()
