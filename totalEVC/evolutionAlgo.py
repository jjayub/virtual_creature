import random as r
from population import Population
import settings
from individual import LongGuy
from utilities import *


def evolvePop(pop, world):
    newPop = Population(pop.getNumOfIndividuals(), False, world)
    # keep best guy
    if (settings.ELITISM):
        best = pop.getFittest()
        b = LongGuy()
        b.setJointData(best.getJointData())
        b.setSegmentDimension(best.getDimensions())
        b.setColors(best.getColors())
        b.setID(best.getID())

        newPop.saveIndividual(b)
        # crossover pop
        elitismOffset = 1
    else:
        elitismOffset = 0
    # Loop over the population size and create new individuals with
    # crossover

    if settings.PARENT_SEL == 'roulette':
        crossNum = pop.getNumOfIndividuals()
    else:
        crossNum = int(pop.getNumOfIndividuals() / 2) + 1

    for i in range(elitismOffset, crossNum):
        if settings.PARENT_SEL == 'roulette':
            indiv1 = rouletteWheelSelection(pop)
            indiv2 = rouletteWheelSelection(pop)
        else:
            indiv1 = tournamentSelection(pop, world)
            indiv2 = tournamentSelection(pop, world)

        while indiv1.getFitness() == indiv2.getFitness():
            if settings.PARENT_SEL == 'roulette':
                indiv2 = rouletteWheelSelection(pop)
            else:
                indiv2 = tournamentSelection(pop, world)

        print("PARENTS ---- MOM: ", indiv1.getID(), " DAD:", indiv2.getID())
        print(indiv1.getFitness())
        print(indiv2.getFitness())
        b1, b2 = crossover(indiv1, indiv2)

        madeDup = True
        while madeDup:
            if newPop.contains(b1) or newPop.contains(b2):
                print("bred a dup")
                if settings.PARENT_SEL == 'roulette':
                    indiv1 = rouletteWheelSelection(pop)
                    indiv2 = rouletteWheelSelection(pop)
                else:
                    indiv1 = tournamentSelection(pop, world)
                    indiv2 = tournamentSelection(pop, world)

                while indiv1.getFitness() == indiv2.getFitness():
                    if settings.PARENT_SEL == 'roulette':
                        indiv2 = rouletteWheelSelection(pop)
                    else:
                        indiv2 = tournamentSelection(pop, world)
                b1, b2 = crossover(indiv1, indiv2)
            else:
                madeDup = False

        bnum = r.randint(0, 1)
        if bnum == 1:
            newPop.setIndividual(i, b1)
        else:
            newPop.setIndividual(i, b2)
        if settings.PARENT_SEL == 'tournament':
            if bnum == 1:
                newPop.setIndividual(int(pop.getNumOfIndividuals() / 2) + i, b2)
            else:
                newPop.setIndividual(int(pop.getNumOfIndividuals() / 2) + i, b1)

    # mutate pop
    for i in range(elitismOffset, newPop.getNumOfIndividuals()):
        mutated = mutate(newPop.getIndividual(i))
        newPop.setIndividual(i, mutated)
    newPop.removeEmpty()
    return newPop


def crossover(mom, dad):
    momNumSegs = mom.getNumSegs()
    dadNumSegs = dad.getNumSegs()

    momSegs = mom.getDimensions()
    momJoints = mom.getJointData()
    momCol = mom.getColors()
    dadCol = dad.getColors()
    dadSegs = dad.getDimensions()
    dadJoints = dad.getJointData()

    momCut = r.randint(0, momNumSegs - 2)  # min length is 2 segments, so child contains both mom and dad
    dadCut = r.randint(0, dadNumSegs - 2)

    baby1Segs = momSegs[0:momCut + 1] + dadSegs[dadCut + 1:dadNumSegs]
    baby2Segs = dadSegs[0:dadCut + 1] + momSegs[momCut + 1:momNumSegs]

    baby1Col = momCol[0:momCut + 1] + dadCol[dadCut + 1:dadNumSegs]
    baby2Col = dadCol[0:dadCut + 1] + momCol[momCut + 1:momNumSegs]

    baby1Joints = momJoints[0:momCut + 1] + dadJoints[dadCut + 1:len(dadJoints)]
    baby2Joints = dadJoints[0:dadCut + 1] + momJoints[momCut + 1:len(momJoints)]

    b1 = LongGuy()
    b1.setJointData(baby1Joints)
    b1.setSegmentDimension(baby1Segs)
    b1.setColors(baby1Col)
    b1.setID(settings.indiv_id)

    settings.indiv_id += 1

    b2 = LongGuy()
    b2.setJointData(baby2Joints)
    b2.setSegmentDimension(baby2Segs)
    b2.setColors(baby2Col)
    b2.setID(settings.indiv_id)

    settings.indiv_id += 1

    return b1, b2


def mutate(indiv):
    segs = indiv.getDimensions()
    joints = indiv.getJointData()
    cols = indiv.getColors()
    for i in range(0, len(segs)):
        if (r.random() <= settings.MUTATION_RATE):
            print("MUTATED ------------------------------------------------------------")
            w = r.uniform(0.1, 4)
            h = r.uniform(0.1, 4)
            segs[i] = (w, h)
            cols[i] = genColor()
    for i in range(0, len(joints)):
        if (r.random() <= settings.MUTATION_RATE):
            print("MUTATED ------------------------------------------------------------")
            lower = degToRad(-1 * r.randint(0, 90))
            upper = degToRad(r.randint(0, 90))
            motorSpeed = degToRad(r.randint(500, 2000))
            joints[i] = (lower, upper, motorSpeed)
    indiv.setColors(cols)
    indiv.setSegmentDimension(segs)
    indiv.setJointData(joints)
    return indiv


def tournamentSelection(pop, world):
    tournamentPop = Population(settings.TOURNAMENT_SIZE, False, world)
    for i in range(0, settings.TOURNAMENT_SIZE):
        randomId = int(r.random() * pop.getNumOfIndividuals())
        tournamentPop.setIndividual(i, pop.getIndividual(randomId))
    fittest = tournamentPop.getFittest()
    return fittest


# rouletteWheelSelection

def get_probability_list(pop):
    indivs = pop.getIndividuals()
    fitness_list = []
    total_fit = 0
    for i in indivs:
        fitness_list.append(i.getFitness())
        total_fit += i.getFitness()
    relative_fitness = [f / total_fit for f in fitness_list]
    probabilities = [sum(relative_fitness[:i + 1])
                     for i in range(len(relative_fitness))]
    return probabilities


def rouletteWheelSelection(pop):
    slice = r.random() * pop.getTotalFitness()
    indivs = pop.getIndividuals()
    total = 0
    for i in indivs:
        total += i.getFitness()
        if total > slice:
            return i
    return indivs[0]
