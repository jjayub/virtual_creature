import random as r
import settings
from utilities import *
import Box2D
from Box2D.b2 import *


class LongGuy(object):
    def __init__(self):
        self.id = None
        self.colors = []
        self.segmentDimensions = []  # (w,h) pair
        self.segments = []
        self.jointData = []
        self.joints = []

    def getID(self):
        return self.id

    def setID(self, id):
        self.id = id

    def getSegments(self):
        return self.segments

    def setSegmentDimension(self, sd):
        self.segmentDimensions = sd

    def getNumSegs(self):
        return len(self.segmentDimensions)

    def getJointData(self):
        return self.jointData

    def setJointData(self, jd):
        self.jointData = jd

    def getDimensions(self):
        return self.segmentDimensions

    def getColors(self):
        return self.colors

    def setColors(self, c):
        self.colors = c

    def generateLongGuy(self):
        self.id = settings.indiv_id
        settings.indiv_id += 1
        numSegs = r.randint(2, 6)
        for i in range(0, numSegs):
            w = r.uniform(0.1, 4)
            h = r.uniform(0.1, 4)
            self.segmentDimensions.append((w, h))
            self.colors.append(genColor())

        for i in range(0, numSegs - 1):
            lower = degToRad(-1 * r.randint(0, 90))
            upper = degToRad(r.randint(0, 90))
            motorSpeed = degToRad(r.randint(500, 1000))
            self.jointData.append((lower, upper, motorSpeed))  # (lower, upper, motorSpeed)

    def addToWorld(self, world):
        for i in range(len(self.segmentDimensions)):
            body = world.CreateDynamicBody(position=settings.BIRTH_POS, angle=degToRad(0))
            fixture = body.CreatePolygonFixture(box=self.segmentDimensions[i], density=1, friction=0.3,
                                                userData=self.colors[i])
            fixture.filterData.maskBits = 0x1
            fixture.filterData.groupIndex = -1
            fixture.filterData.categoryBits = 0x2
            self.segments.append(body)

        for i in range(len(self.segmentDimensions) - 1):
            (w, h) = self.segmentDimensions[i]
            (w2, h2) = self.segmentDimensions[i + 1]
            joint = world.CreateRevoluteJoint(bodyA=self.segments[i], bodyB=self.segments[i + 1],
                                              localAnchorA=(w / 2, 0),
                                              localAnchorB=(-1 * w2 / 2, 0))
            joint.motorEnabled = True
            joint.maxMotorTorque = 1000
            joint.limitEnabled = True
            joint.lowerLimit = self.jointData[i][0]
            joint.upperLimit = self.jointData[i][1]
            joint.motorSpeed = self.jointData[i][2]  # degree per sec
            self.joints.append(joint)

    def getFitness(self):  # horizontal distance moved right
        x = self.getLeftMostPosition()
        return x  # - settings.BIRTH_POS[0]

    def getRightMostPosition(self):
        """
        :return: x_coord : right most x position
        """
        x_coord = -1000000
        for body in self.segments:
            for fixture in body.fixtures:
                vertices = [(body.transform * v) for v in fixture.shape.vertices]
                for x, y in vertices:
                    if x > x_coord:
                        x_coord = x
        return x_coord

    def getLeftMostPosition(self):
        """
        :return: x_coord : left most x position
        """
        x_coord = 1000000
        for body in self.segments:
            for fixture in body.fixtures:
                vertices = [(body.transform * v) for v in fixture.shape.vertices]
                for x, y in vertices:
                    if x < x_coord:
                        x_coord = x
        return x_coord

    def print(self):
        print(len(self.segments), " ", len(self.segmentDimensions), "  ", len(self.joints), " ", len(self.jointData),
              " ")

    def destroyIndiv(self, world):
        for joint in self.joints:
            world.DestroyJoint(joint)
        for segment in self.segments:
            world.DestroyBody(segment)
