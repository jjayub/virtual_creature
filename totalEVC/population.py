from individual import LongGuy
import utilities


class Population(object):
    def __init__(self, size, initialise, world):
        self.individuals = [LongGuy()] * size
        self.numOfIndividuals = size

        if initialise:
            for i in range(0, size):
                a = LongGuy()
                a.generateLongGuy()
                a.addToWorld(world)
                self.individuals[i] = a

    def contains(self, indiv):
        for i in self.individuals:
            if i.getDimensions() == indiv.getDimensions():
                if i.getJointData() == indiv.getJointData():
                    return True
        return False

    def getBodyCount(self):
        total = 0
        for i in self.individuals:
            total += i.getNumSegs()
        return total

    def addIndividualsToWorld(self, world):
        for i in range(0, self.numOfIndividuals):
            self.individuals[i].addToWorld(world)

    def getIndividual(self, index):
        return self.individuals[index]

    def getIndividuals(self):
        return self.individuals

    def getTotalFitness(self):
        total = 0
        for i in self.individuals:
            total += i.getFitness()
        return total

    def getFittest(self):
        fittest = self.individuals[0]
        size = self.getNumOfIndividuals()
        for i in range(1, size):
            if fittest.getFitness() <= self.individuals[i].getFitness():
                fittest = self.individuals[i]
        return fittest

    def saveIndividual(self, indiv: LongGuy) -> None:
        """append it to individuals list"""
        self.individuals.append(indiv)

    def saveIndividualOnFile(self, indiv: LongGuy) -> None:
        """append it to individuals list"""
        self.individuals.append(indiv)
        self.numOfIndividuals += 1

    def setIndividual(self, index, indiv):
        self.individuals[index] = indiv

    def getNumOfIndividuals(self):
        return len(self.individuals)

    def removeEmpty(self):
        for i in self.individuals:
            if i.getNumSegs() == 0:
                self.individuals.remove(i)

    def destroyPop(self, world):
        for indiv in self.individuals:
            indiv.destroyIndiv(world)

    def savePopToFile(self):
        filename = 'longguy_popData/pop_' + utilities.getTestNum() + '.txt'
        f = open(filename, 'w')
        for i in self.individuals:
            f.write(str(i.getDimensions()) + '\n')
            f.write(str(i.getJointData()) + '\n')
            f.write(str(i.getColors()) + '\n\n')

    def savePopToFileGiveName(self, filename):
        f = open(filename, 'w')
        for i in self.individuals:
            f.write(str(i.getDimensions()) + '\n')
            f.write(str(i.getJointData()) + '\n')
            f.write(str(i.getColors()) + '\n\n')

    def getPopRightMostPosition(self):
        """Return right most position (x-coordinate) of this population"""
        x_right = -1000000000
        for i in self.individuals:
            x_temp = i.getRightMostPosition()
            if x_temp > x_right:
                x_right = x_temp
        return x_right

    def getPopLeftMostPosition(self):
        """Return left most position (x-coordinate) of this population"""
        x_left = -1000000000
        for i in self.individuals:
            x_temp = i.getLeftMostPosition()
            if x_temp < x_left:
                x_left = x_temp
        return x_left
