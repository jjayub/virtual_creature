SCREEN_WIDTH = 1875
SCREEN_HEIGHT = 500

# --- constants ---
# Box2D deals with meters, but we want to display pixels,
# so define a conversion factor:
PPM = 20.0  # pixels per meter
TARGET_FPS = 30
TIME_STEP = 1.0 / TARGET_FPS

BIRTH_POS = (10,10)

indiv_id = 0

POP_SIZE = 21  # must be an odd number

SIM_STEPS = 400

CROSSOVER_RATE = 0.7

MUTATION_RATE = 0.025

UNIFORM_RATE = 0.5

TOURNAMENT_SIZE = 5

ELITISM = True

PARENT_SEL = 'roulette'

RUN_UNTIL_GEN = 30


