#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
An attempt at some simple, self-contained pygame-based examples.

Example 01

In short:
One static body: a big polygon to represent the ground
One dynamic body: a rotated big polygon
And some drawing code to get you going.

kne
"""
import pygame
from population import *
from settings import *
from utilities import *
from pygame.locals import (QUIT, KEYDOWN, K_ESCAPE)
import evolutionAlgo as EA

import Box2D  # The main library
# Box2D.b2 maps Box2D.b2Vec2 to vec2 (and so on)
from Box2D.b2 import (world, polygonShape, staticBody, dynamicBody)
import individual

# --- pygame setup ---
pygame.font.init()
description = pygame.font.SysFont('tahoma', 30)
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT), 0, 32)
pygame.display.set_caption('KangJay virtual creature guys')
clock = pygame.time.Clock()

# --- pybox2d world setup ---
# Create the world
world = world(gravity=(0, -30), doSleep=True)

# And a static body to hold the ground shape and walls
ground_body = world.CreateStaticBody(
    position=(0, 0),
    shapes=polygonShape(box=(SCREEN_WIDTH * 100 / PPM, 1)),
)

left_wall = world.CreateStaticBody(
    position=(0, 0),
    shapes=polygonShape(box=(0.1, 40)),
)


def loadPopFromFileGiveName(filename):
    f = open("jay/" + filename, 'r')
    individuals = []

    import ast

    while True:
        line = f.readline()

        if line == "":
            break
        if line != "\n":
            indiv = LongGuy()
            indiv.setSegmentDimension(ast.literal_eval(line.strip()))
            indiv.setJointData(ast.literal_eval(f.readline().strip()))
            indiv.setColors(ast.literal_eval(f.readline().strip()))
            individuals.append(indiv)

    f.close()

    pop = Population(0, False, world)
    for i in individuals:
        pop.saveIndividualOnFile(i)
    pop.addIndividualsToWorld(world=world)

    return pop


def runSim(runNum, foldername):
    genCount = 1
    print("generation :", genCount)
    # pop = Population(POP_SIZE, True, world)
    # pop.savePopToFileGiveName(foldername + "/" + "run" + str(runNum) + "_startPop.txt")
    pop = loadPopFromFileGiveName(filename="run4_endPop.txt")

    running = True
    pause = False

    worldSteps = 0
    maxFit = 0

    screen_left = 0
    screen_right = SCREEN_WIDTH

    x_offset = 0
    while running:
        # worldSteps += 1
        # if worldSteps > SIM_STEPS and worldSteps < 502:
        #     genMax = pop.getFittest().getFitness()
        #     saveFitness(foldername + "/" + "run" + str(runNum) + "_fitness.txt", genCount,
        #                 genMax)  # save max fitness of generation
        #     if genCount == RUN_UNTIL_GEN:
        #         pop.savePopToFileGiveName(foldername + "/" + "run" + str(runNum) + "_endPop.txt")
        #         pop.destroyPop(world)
        #         break
        #     else:
        #         worldSteps = 0
        #         if genMax > maxFit:
        #             maxFit = genMax
        #         Newpop = EA.evolvePop(pop, world)
        #         Newpop.addIndividualsToWorld(world)
        #         genCount += 1
        #         print("generation :", genCount)
        #         pop.destroyPop(world)
        #         pop = Newpop

        for event in pygame.event.get():
            if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
                # The user closed the window or pressed escape
                pygame.quit()
            elif event.type == KEYDOWN and event.key == pygame.K_p:
                pause = True
                while pause:
                    for e in pygame.event.get():
                        if e.type == KEYDOWN and e.key == pygame.K_p:
                            pause = False
            elif event.type == KEYDOWN and event.key == pygame.K_s:
                pop.savePopToFile()
                print("Saved Generation: " + str(genCount))

        # set camera to follows the right most creature
        right_indiv = int(pop.getPopRightMostPosition() * PPM)
        space_gab = int(SCREEN_WIDTH * 0.2)
        if right_indiv > screen_right - space_gab:
            # -1 is use to move scene to left to make it looks like move to right
            x_offset = (right_indiv - (screen_right - space_gab)) * -1
            # camera (right border) should equal to actual screen + offset
            screen_right = (x_offset * -1) + screen_right
            screen_left = (x_offset * -1) + screen_left
        elif right_indiv < screen_left + space_gab:
            x_offset = ((screen_left + space_gab) - right_indiv)
            screen_right = screen_right - x_offset
            screen_left = screen_left - x_offset

        screen.fill((136, 154, 173, 0))
        for body in world.bodies:
            # The body gives us the position and angle of its shapes
            for fixture in body.fixtures:
                shape = fixture.shape
                vertices = [(body.transform * v) * PPM for v in shape.vertices]
                # to handle "Type Error: points must be number pairs"
                vertices = [(int(v[0]) + (screen_right - SCREEN_WIDTH) * -1, int(v[1])) for v in vertices]
                vertices = [(v[0], SCREEN_HEIGHT - v[1]) for v in vertices]
                if body == ground_body:
                    pygame.draw.polygon(screen, (255, 255, 255, 255), vertices)
                elif body == left_wall:
                    pygame.draw.polygon(screen, (136, 154, 173, 0), vertices)
                else:
                    pygame.draw.polygon(screen, fixture.userData, vertices)
                    pygame.draw.polygon(screen, (255, 255, 255, 255), vertices, 1)
        for joint in world.joints:
            if (joint.motorSpeed < 0 and joint.angle < joint.lowerLimit) or \
                    (joint.motorSpeed > 0 and joint.angle > joint.upperLimit) or \
                            joint.speed == 0:
                joint.motorSpeed *= -1

        # display text on screen
        text = 'Generation: ' + str(genCount)
        text2 = 'Run Max Fitness: ' + str(int(maxFit))
        descriptionArea = description.render(text, True, (0, 0, 0))
        more = description.render(text2, True, (0, 0, 0))
        screen.blit(descriptionArea, (3, 3))
        screen.blit(more, (3, 30))

        world.Step(TIME_STEP, 6, 2)  # 10,10

        # Flip the screen and try to keep at the target FPS
        pygame.display.flip()

        clock.tick(TARGET_FPS)

        # pygame.quit()


for i in range(10):
    runSim(i, "jay")
