import math
import random
# for graphing
import matplotlib.pyplot as plt


def degToRad(d):
    return d * (math.pi / 180)


def radToDeg(r):
    return r * (180 / math.pi)


def genColor():
    return (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255), 255)


def saveToNewFile(filename, text):
    f = open(filename, 'w')
    f.write(text)
    f.close()


def addToFile(filename, text):
    f = open(filename, 'a')
    f.write(text)
    f.close()


def getTestNum():
    # get current test number
    with open('longguy_popData/testnum.txt') as file:
        data = file.read()
    # update test number
    file = open('longguy_popData/testnum.txt', 'w')
    file.write(str(int(data) + 1))
    file.close()
    return data


def saveFitness(filename, gen, fitness):
    file = open(filename, 'a')
    t = '\n' + str(gen) + ',' + str(fitness)
    file.write(t)
    file.close()
